package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class EditLead extends ProjectMethods {
	
	@BeforeTest(groups = {"smoke"})
	public void setdata() {
		testCaseName="TC002_EditLead";
		testCaseDesc="Edit a lead";
		author = "sudha";
		category = "Smoke";
		
	}
	
	@Test(groups = {"smoke"})//(dependsOnMethods = {"testcase.TC002_CreateLead.createLead"},enabled = false)
   	public void editLead() {
	WebElement elecreatelead = locateElement("linktext","Create Lead");
	click(elecreatelead);
	WebElement elecname = locateElement("id","createLeadForm_companyName");
	type(elecname,"priya&Co");
	WebElement elefirstname = locateElement("id","createLeadForm_firstName");
	type(elefirstname,"Priya");
	WebElement elelastname = locateElement("id","createLeadForm_lastName");
	type(elelastname,"Singh");
	WebElement elesubmit = locateElement("name","submitButton");
	click(elesubmit);
	
}
}
