package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
	
	@BeforeTest(groups = {"smoke"})
	public void setdata() {
		testCaseName="TC002_CreateLead";
		testCaseDesc="create a new lead";
		author = "sudha";
		category = "Smoke";
		
	}
	@Test(groups = {"smoke"})
		//(dependsOnMethods = {"testcase.TC001_LoginAndLogOut.login"})
	public void createLead() {
	WebElement elecreatelead = locateElement("linktext","Create Lead");
	click(elecreatelead);
	WebElement elecname = locateElement("id","createLeadForm_companyName");
	type(elecname,"priya&Co");
	WebElement elefirstname = locateElement("id","createLeadForm_firstName");
	type(elefirstname,"Priya");
	WebElement elelastname = locateElement("id","createLeadForm_lastName");
	type(elelastname,"Singh");
	WebElement elesubmit = locateElement("name","submitButton");
	click(elesubmit);
	
}
	
}
