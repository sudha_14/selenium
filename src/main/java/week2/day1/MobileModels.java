package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MobileModels {

	public static void main(String[] args) {
		List<String> phones = new ArrayList<String>();
		phones.add("oppo");
		phones.add("Vivo");
		phones.add("nokia");
		phones.add("Samsung");
		phones.add("Vivo");
		phones.add("Iphone");
		
		int size = phones.size();
		System.out.println("Count of mobilemodels:"+size);
		for(String eachPhone:phones) {
			System.out.println("mobilemodels:" +eachPhone);
		}
		System.out.println("Last phone is:" +phones.get(size-2));
		Collections.sort(phones);
		System.out.println(phones);
		Collections.reverse(phones);
		System.out.println(phones);
				
	}

}
