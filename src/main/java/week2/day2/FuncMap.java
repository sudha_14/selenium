package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class FuncMap {

	public static void main(String[] args) {
	String name = "Suganya";
	char[] ch = name.toCharArray();

	//verifying how many time each character is ocurring, so we have to use below map
	Map<Character,Integer> m1 = new HashMap<Character,Integer>();
	
		for(char c1 : ch) {
			if (m1.containsKey(c1)) {
			Integer	val = m1.get(c1)+1;
			m1.put(c1, val);
			}else {
				m1.put(c1, 1);
			}
			
			
			}
		System.out.println(m1);
			}
		
	}


