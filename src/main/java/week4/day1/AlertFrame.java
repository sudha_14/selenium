package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertFrame {

			public static void main(String[] args) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.navigate().to("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
			//driver.manage().window().maximize();
			driver.switchTo().frame("iframeResult");
			driver.findElementByXPath("//button[text()='Try it']").click();
			driver.switchTo().alert().sendKeys("Sudha");
						driver.switchTo().alert().accept();
			
	}

}
