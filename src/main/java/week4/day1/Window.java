package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class Window {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> wh = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(wh);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		//to take screenshot
		File sc = driver.getScreenshotAs(OutputType.FILE);
		File dc = new File("./snaps/ps.img");
		FileUtils.copyFile(sc, dc);
		wh = driver.getWindowHandles();
		lst = new ArrayList<>();
		lst.addAll(wh);
		driver.switchTo().window(lst.get(0));
		driver.close();
		
		

	}

}
