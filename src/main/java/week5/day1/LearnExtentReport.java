package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReport {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true); // to know the execution history details if it's false then will erase the history
		ExtentReports extent = new ExtentReports(); // to convert the readable html file to writeable file
		extent.attachReporter(html);
//test case level
		ExtentTest test = extent.createTest("TC001_CreateLead", "Create a new Lead");
		test.assignCategory("Smoke Test");
		test.assignAuthor("Sudha");
//test step level
		test.pass("Browser launched successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/ps.img").build());
	    test.pass("Demoslaesmanager clikcked successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/ps.img").build());
	test.pass("Password entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/ps.img").build());
	extent.flush();//this is mandatory method to execute extent reports
		
	}

}
