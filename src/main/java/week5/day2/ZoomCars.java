package week5.day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class ZoomCars extends SeMethods
{
	@Test
	public void carapp() throws InterruptedException{
		startApp("chrome","https://www.zoomcar.com/chennai/");
		WebElement ele = locateElement("linkText", "Start your wonderful journey");
		click(ele);
		WebElement ele1 = locateElement("xpath", "//div[@class='items'][3]");
		click(ele1);
		WebElement ele2 = locateElement("xpath", "//button[@class = 'proceed']");
		click(ele2);
		Date dt = new Date();
		DateFormat df = new SimpleDateFormat("dd");
		String today = df.format(dt);
		int dt1 = Integer.parseInt(today)+1;
		System.out.println("Tomorrow:"+dt1);
		WebElement nextDay = driver.findElementByXPath("//div[contains(text(),'"+dt1+"')]");
		click(nextDay);	
		WebElement next = locateElement("xpath", "//button[@class='proceed']");
		click(next);
		WebElement done = locateElement("xpath", "//button[@class='proceed']");
		click(done);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int maxPrice = 0;
		String highestPricedCar = "";
		List<WebElement> cars = driver.findElementsByXPath("//div[@class= 'details']/h3");
		List<WebElement> priceofcars = driver.findElementsByXPath("//div[@class='price']");
		for (int i= 0;i<cars.size();i++) {
			int price = Integer.parseInt(priceofcars.get(i).getText().replaceAll("\\D",""));
			if(price > maxPrice) {
				maxPrice = price;
				highestPricedCar = cars.get(i).getText();
			}
		}
		System.out.println("The max Priced car is "+highestPricedCar);
		WebElement bookcar = locateElement("xpath", "//div[contains(text(),'"+maxPrice+"')]//following::button[text()='BOOK NOW'][1]");
         Thread.sleep(2000);
		click(bookcar);
	
	}



}