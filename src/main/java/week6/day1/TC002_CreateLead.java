package week6.day1;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import week6.day2.FetchingExcel;

public class TC002_CreateLead extends ProjectMethods {
	
	@BeforeTest(groups = {"smoke"})
	public void setdata() {
		testCaseName="TC002_CreateLead";
		testCaseDesc="create a new lead";
		author = "sudha";
		category = "Smoke";
		
	}
	@Test(dataProvider="Positive")
		//(dependsOnMethods = {"testcase.TC001_LoginAndLogOut.login"})
	public void createLead(String cname, String fName, String lName) {
	WebElement elecreatelead = locateElement("linktext","Create Lead");
	click(elecreatelead);
	WebElement elecname = locateElement("id","createLeadForm_companyName");
	type(elecname,cname);
	WebElement elefirstname = locateElement("id","createLeadForm_firstName");
	type(elefirstname,fName);
	WebElement elelastname = locateElement("id","createLeadForm_lastName");
	type(elelastname,lName);
	WebElement elesubmit = locateElement("name","submitButton");
	click(elesubmit);
	
}
	
}
