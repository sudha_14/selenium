package week6.day2;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchingExcel {
	
	public static Object[][] getExcelData(String fileName) throws IOException {
		String stringCellValue = null;
		XSSFWorkbook wbook = new XSSFWorkbook("./excel/filename.xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
         int RowCount = sheet.getLastRowNum();	
         System.out.println("Row Count = "+RowCount);
         int columnCount = sheet.getRow(0).getLastCellNum();
			System.out.println("ColumnCount= " + columnCount);
			Object[][] data = new Object[RowCount][columnCount];
			for (int i = 1; i <= RowCount; i++) {
				XSSFRow row = sheet.getRow(i);
				for (int j = 0; j < columnCount; j++) {
					XSSFCell cell = row.getCell(j);
					
					if(cell.getCellTypeEnum()==CellType.STRING) {
						 data[i-1][j] = cell.getStringCellValue();	
					} else if(cell.getCellTypeEnum()==CellType.NUMERIC) {
						 data[i-1][j] = ""+(int)cell.getNumericCellValue();
					}
					
						System.out.println(stringCellValue);
						}
         
						}
			return data;
	}

}
